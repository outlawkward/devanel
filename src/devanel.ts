const ansi = require('ansi')
    , cursor = ansi(process.stdout)

export default () => {
  cursor.cyan().write('Devanel!\n')
}

export const ansiTest = () => new Promise((resolve: any) => {
  cursor.cyan().write("Devanel ").red().write("is terrible!")
  resolve()
})
  .then(sleeper(1000))
  .then(() => {
    for (let i = 0; i < 12; i++) {
      cursor.back()
    }

    cursor.cyan().write("works great!").reset().write('\n')
  })

const sleeper = (waitTime: number) => (x: any) => new Promise((resolve: any) => setTimeout(() => resolve(x), waitTime))