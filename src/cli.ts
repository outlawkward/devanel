import * as minimist from 'minimist'

const argv = minimist(process.argv.slice(2))

if (argv.help || argv.h) {
  const pkg = require('../package.json')
  console.log(`${pkg.name} v${pkg.version}`)
  console.log(pkg.description)
} else if (argv.ansiTest || argv.T) {
  const ansiTest = require('./devanel').ansiTest
  ansiTest()
}