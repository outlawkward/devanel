const { resolve } = require('path')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')

module.exports = {
  entry: {
    index: './src/devanel.ts',
    cli: './src/cli.ts',
  },

  mode: 'development',

  target: 'node',

  externals: [nodeExternals()],

  devtool: 'inline-source-map',

  resolve: {
    extensions: ['.ts', '.js'],
  },

  module: {
    rules: [
      { test: /\.ts$/, loader: 'ts-loader', exclude: /node_modules/ },
    ],
  },

  plugins: [
    new webpack.BannerPlugin({ banner: '#!/usr/bin/env node', raw: true, include: /cli\.js/ })
  ],

  output: {
    path: resolve(__dirname, 'dist'),
    filename: '[name].js'
  }
}